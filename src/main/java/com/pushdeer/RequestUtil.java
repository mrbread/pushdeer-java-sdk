package com.pushdeer;

import okhttp3.OkHttpClient;
import okhttp3.Request;
import okhttp3.Response;

import java.io.IOException;

/**
 * author: liuchangrong
 * date: 2022-02-24 23:50
 * description: 发送请求工具类
 */
public class RequestUtil {
    private static OkHttpClient client;
    static {
        client = new OkHttpClient();
    }
    /**
     * 发送消息请求
     * @param url
     * @return
     * @throws IOException
     */
    public static String sendRequest(String url) throws IOException {
        Request request = new Request.Builder()
                .url(url)
                .build();
        try (Response response = client.newCall(request).execute()) {
            return response.body().string();
        }
    }
}
