package com.pushdeer;

/**
 * author: liuchangrong
 * date: 2022-02-25
 * description: 发送消息之后的响应体
 */
public class RequestResponse {
    /**
     * 响应码 0 代表成功
     */
    private int code;
    /**
     * 错误信息
     */
    private String error;

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getError() {
        return error;
    }

    public void setError(String error) {
        this.error = error;
    }
}

