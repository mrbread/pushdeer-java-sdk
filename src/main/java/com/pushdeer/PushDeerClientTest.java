package com.pushdeer;

/**
 * author: liuchangrong
 * date: 2022-02-24 23:30
 * description: 测试发送消息是否正常
 */
public class PushDeerClientTest {
    public static void main(String[] args) throws Exception{
        String pushKey = "your push key here";
        PushDeerClient client = new PushDeerClient(pushKey);
        client.sendTextMessage("第一条文字消息", "optional description");
        client.sendMarkDownMessage("# 第一条markdown消息", "**optional** description in markdown");
        client.sendImageMessage("http://www.ruanyifeng.com/images_pub/pub_182.jpg",
                "图片备注");
    }
}
