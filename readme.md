## PushDeer sdk for java

### intro

**current implemented**

custom set pushdeer server host and self pushkey to send text、image、markdown
message.

### Usage
you can execute the following command int the project root directory:
```ssh
mvn clean install
```
to install the jar file in your local maven repository.

Then you can add the following info into your other project:
```xml
        <dependency>
            <groupId>com.pushdeer</groupId>
            <artifactId>pushdeer-java-sdk</artifactId>
            <version>1.0.0</version>
        </dependency>
```

Happy Coding!

### Inspired By

https://github.com/gaoliang/pypushdeer

Respect

### TODO

Have not been decided yet.
Maybe I can learn from the go sdk and implement more interfaces.


### PS
if you have any idea, please let me know.
